以下是 SIGer 向 RVWeekly 发出的合刊邀约，对 SIGer 的定位和工作做了介绍，最终实现了 RISC-V 专题的共同编纂。

### - SIGer 是什么？

https://gitee.com/flame-ai/siger  
这是我们的样刊，寒假开始进驻 openEuler 带同学们学开源，受益良多，  
SIGer 兴趣小组 是最好的科普阵地，向青少年宣传开源文化的桥头堡。  
需要更多同学参与和老师们的支持。

### - SIGer 编委会是什么？

SIGer 作为期刊，主要力量是同学们，叫编委会  
SIGer 还有一个重要的岗位，编审，就是辅导老师，对于在职的专家和工程师，对于学生来说就是编审。

### - SIGer 以专题立刊，如何选题？

我们的思路是以专题方式立刊，也就是不同的方向，作为兴趣小组的内容源。  
RISC-V 是前段时间同学们收集的题目，符合当前，我国科技前哨阵地的情况。芯片的核心 CPU 更是重中之重。虽然还很肤浅和外围，但这是一个非常好的学习入口。借由这期 “芯” 专刊的发布和整理，同学们将会是直接受益人，也会带动更多青少年关注这个科技最前沿。

### - SIGer 参与者是什么年龄段的？

高中，大学。也有偏低龄的初中生。  
RISC-V还是高中和大学为主。不同期刊开卷有益。  
我是主张不限制年龄。不主观。看懂多少就学多少。

### - SIGer 如何与 RVWeekly 合刊一期青少年增刊？

1. RVWeekly 和 SIGer 合刊一期青少年增刊。可以组成一个 专题学习小组，会涌现骨干志愿者，能够协助 RVWeekly 的一些日常工作。
2. 从 RVWeekly 的内容组织形式，和 SIGer 的基本形式一致，都是网摘，只是我们没有严格按照周刊月刊方式组稿。无时限主题方式，合刊的形式，由辅导员担任 编审，领导同学们一起学习。
3. 这是一个很有意义的项目，需要更专业的支持，可以引荐如芯来，RISC-V联盟相关老师。这是 SIGer 除了编委，编审的另一个重要的角色，SIGer 大使（行业专家）

### - SIGer "芯"专刊的内容源？

https://mp.weixin.qq.com/s?__biz=Mzg4NTA2NzE0Mg==&mid=2247503232&idx=1&sn=7683ff974822137b7d6c89d7064dba3d  
芯片设计公司修炼的“四层境界”，中国公司属于哪一层？

https://mp.weixin.qq.com/s?__biz=MzI4NDY1MDg0MA==&mid=2247483731&idx=2&sn=cda0d55f3dd62723ff09e324cab9c4b4  
AMD发布最新7nm服务器芯片，英特尔很紧张？

https://mp.weixin.qq.com/s?__biz=MzI5ODM3Mzk4Ng==&mid=2247483769&idx=1&sn=1bf5d4970e599a2c2e4247bcf1e4b431  
RISC-V的开源诱惑

https://mp.weixin.qq.com/s?__biz=MjM5MTIwMjY1Mg==&mid=2649926460&idx=1&sn=2da6c2d549115ec71e1b7e373ec0b4f9  
Arm的十年PC征程，和微软的“暧昧”

RISC-V, China, Nightingales RISC-V，中国，夜莺 #[note_4485640](https://gitee.com/yuandj/siger/issues/I3B6OG#note_4485640_link)  
https://interconnected.blog/riscv-china-nightingales/

【欧来】新康众开源 openEuler on RISC-V  
https://gitee.com/yuandj/siger/issues/I37ZOL

最早我收到 RISC-V 的 OPENEULER 的 SIG组 MAILLIST .  
然后逐步越来越多这方面的信息。来自朋友圈的信息，家长老师的，也请教过一些问题。
